
/**
 * Wrapper of LocalStorage
 */
function setLocalStorage(fieldName, value) {
	window.localStorage.setItem(fieldName, JSON.stringify(value));
    //window.localStorage.setItem(fieldName, value);
}
function getLocalStorage(fieldName) {
	if(window.localStorage.getItem(fieldName) == null) {
		return null;
	}
	return JSON.parse(window.localStorage.getItem(fieldName));	
}
function removeLocalStorage(fieldName) {
	window.localStorage.removeItem(fieldName);
}
function clearLocalStorage() {
	window.localStorage.clear();		
}
/**
 * Wrapper of sessionStorage
 */
function setSessionStorage(fieldName, value) {
	window.sessionStorage.setItem(fieldName, JSON.stringify(value));	
}
function getSessionStorage(fieldName) {
	return JSON.parse(window.sessionStorage.getItem(fieldName));	
}
function removeSessionStorage(fieldName) {
	window.sessionStorage.removeItem(fieldName);
}
function clearSessionStorage() {
	window.sessionStorage.clear();		
}


/*version 1.1*/
	// json string to array
	function jStrToArray(data){
		return JSON.parse('[' + data + ']');		
	}

//--------------------------------
//--------------------------------
function getAllItems() {
    return window.localStorage.getItem().items;
}